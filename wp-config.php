<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'FORCE_SSL_ADMIN', true ); // Force SSL for Dashboard - Security > Settings > Secure Socket Layers (SSL) > SSL for Dashboard
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

define('DB_NAME', 'admin_alcc');

/** MySQL database username */
define('DB_USER', 'admin_alcc');

/** MySQL database password */
define('DB_PASSWORD', 'OpPDy3tNRw');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'xC!Blk9I5O#$<=%[429l n^YUyzRGy<C-T<@vB&8 4f|hp3cO)S}^xHPCpCZHU/;');
define('SECURE_AUTH_KEY',  'Yp;tyio<}0I2bVbh:!* J.Gp^x#HKlQjM$wd!EhxWuH<gQA_Q~3@l@[Tg QkAxIg');
define('LOGGED_IN_KEY',    'cB/x2DfwMISNDcfYt!yxHyCz;=3FpR#?TAmQdf4LH:XRha;R1Cw3~t1`OwTVl~<C');
define('NONCE_KEY',        'xqg1n_Z:,wEx$SlL^P).P?Yb-o0p=MJ;s#[_<|v8:B>6erB/O~GoS^h^DN?wx]Y+');
define('AUTH_SALT',        '^_k#0lLm..*KTIxp#,6wqqqk(ej_iT:od#u:kVFaSZ/yD$G*^&DyD>|5>ME$O-&_');
define('SECURE_AUTH_SALT', 'iSx,ZU0lr|{oZzq7yk8X[{5CJewv7jv`l+GNci<),7!:kE65(8SL?Atw:|Pr2 SW');
define('LOGGED_IN_SALT',   'c56}eV,BRG,#P( MiP+>YwlW9zR;og=Pcba`hLSw$3C[%O7rREzr@.:Z0E$vLl)q');
define('NONCE_SALT',       'y5D^rs}_bDHt-dOC[A/]}7!NwW=%Y- w@c%0#N;i[>.7pz6Z;YH=r%<M=;c1C1$-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ekhva2d3_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/** Wordpress version auto-update */
define( 'WP_AUTO_UPDATE_CORE', false );