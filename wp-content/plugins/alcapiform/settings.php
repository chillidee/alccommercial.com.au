<?php

class ALCAPIFormSettingsPage {

    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct() {
        add_action('admin_menu', array($this, 'add_plugin_page'));
        add_action('admin_init', array($this, 'page_init'));
    }

    /**
     * Add options page
     */
    public function add_plugin_page() {
        // This page will be under "Settings"
        add_options_page(
                'Settings Admin', 'ALC API Form', 'manage_options', 'alcapiform-setting-admin', array($this, 'create_admin_page')
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page() {
        // Set class property
        $this->options = get_option('alcapiform_option_name');
        ?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2>ALC API Form for Money Maker Settings</h2>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields('alcapiform_option_group');
                do_settings_sections('alcapiform-setting-admin');
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init() {

        register_setting(
                'alcapiform_option_group', // Option group
                'alcapiform_option_name', // Option name
                array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
                'setting_section_id', // ID
                'Global Default Settings', // Title
                array($this, 'print_section_info'), // Callback
                'alcapiform-setting-admin' // Page
        );

        add_settings_section(
            'log_section_id', // ID
            'Log Settings', // Title
            array($this, 'print_log_section'), // Callback
            'alcapiform-setting-admin' // Page
        );

        add_settings_field(
                'geturl', 'API Get URL without Company Code', array($this, 'geturl_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'posturl', 'API Post URL', array($this, 'posturl_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'cid', 'CID', array($this, 'cid_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        /*
          add_settings_field(
          'svs',
          'SVS',
          array( $this, 'svs_callback' ),
          'alcapiform-setting-admin',
          'setting_section_id'
          );

          add_settings_field(
          'mode',
          'Mode',
          array( $this, 'mode_callback' ),
          'alcapiform-setting-admin',
          'setting_section_id'
          );
         */

        add_settings_field(
                'css', 'Enquiry Form CSS Full URL', array($this, 'css_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'jquery', 'jQuery Full URL only if jQuery was not loaded', array($this, 'jquery_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'privacy_text', 'Privacy Text', array($this, 'privacy_text_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'privacy', 'Privacy Page Url', array($this, 'privacy_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'privacy_link_text', 'Privacy Link Text', array($this, 'privacy_link_text_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'Send emails to third party', 'Send emails to third party', array($this, 'send_emails_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'pssurl', 'Partner Success URL', array($this, 'pssurl_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'partner_email', 'Partner email address', array($this, 'partner_email_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'from_email', 'From email address', array($this, 'from_email_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
                'company_name', 'Company name to appear on emails', array($this, 'company_name_callback'), 'alcapiform-setting-admin', 'setting_section_id'
        );

        add_settings_field(
            'record_leads', 'Record leads in CSV file (log)', array($this, 'record_leads_callback'), 'alcapiform-setting-admin', 'log_section_id'
        );
        
        add_settings_field(
                'send_email_lead_fail', 'Send email when lead fails', array($this, 'send_email_lead_fail_callback'), 'alcapiform-setting-admin', 'log_section_id'
        );	
        
        add_settings_field(
                'email_lead_fail', 'To email address', array($this, 'email_lead_fail_callback'), 'alcapiform-setting-admin', 'log_section_id'
        );
        
        add_settings_field(
                'ignore_customers_lead_fail', 'Ignore leads by First Name', array($this, 'ignore_customers_lead_fail_callback'), 'alcapiform-setting-admin', 'log_section_id'
        );	
        
        add_settings_field(
                'ignore_emails_lead_fail', 'Ignore leads that contains emails', array($this, 'ignore_emails_lead_fail_callback'), 'alcapiform-setting-admin', 'log_section_id'
        );

        add_settings_section(
                'setting_section_id_usage', // ID
                'Usage Instruction', // Title
                array($this, 'print_section_info_usage'), // Callback
                'alcapiform-setting-admin' // Page
        );

        add_settings_section(
                'setting_section_id_shortcode', // ID
                'Override Instruction', // Title
                array($this, 'print_section_info_shortcode'), // Callback
                'alcapiform-setting-admin' // Page
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input) {
        $new_input = array();

        if (isset($input['geturl']))
            $new_input['geturl'] = sanitize_text_field($input['geturl']);

        if (isset($input['posturl']))
            $new_input['posturl'] = sanitize_text_field($input['posturl']);

        if (isset($input['cid']))
            $new_input['cid'] = sanitize_text_field($input['cid']);

        if (isset($input['svs']))
            $new_input['svs'] = sanitize_text_field($input['svs']);

        if (isset($input['mode']))
            $new_input['mode'] = sanitize_text_field($input['mode']);

        if (isset($input['css']))
            $new_input['css'] = sanitize_text_field($input['css']);

        if (isset($input['jquery']))
            $new_input['jquery'] = sanitize_text_field($input['jquery']);

        if (isset($input['privacy']))
            $new_input['privacy'] = sanitize_text_field($input['privacy']);

        if (isset($input['privacy_link_text']))
            $new_input['privacy_link_text'] = sanitize_text_field($input['privacy_link_text']);

        if (isset($input['privacy_text']))
            $new_input['privacy_text'] = sanitize_text_field($input['privacy_text']);

        if (isset($input['pssurl']))
            $new_input['pssurl'] = sanitize_text_field($input['pssurl']);

        if (isset($input['partner_email']))
            $new_input['partner_email'] = sanitize_text_field($input['partner_email']);

        if (isset($input['from_email']))
            $new_input['from_email'] = sanitize_text_field($input['from_email']);

        if (isset($input['company_name']))
            $new_input['company_name'] = sanitize_text_field($input['company_name']);

        if (isset($input['send_emails']))
            $new_input['send_emails'] = $input['send_emails'];

        if (isset($input['record_leads']))
            $new_input['record_leads'] = $input['record_leads'];
		
		if (isset($input['send_email_lead_fail']))
            $new_input['send_email_lead_fail'] = $input['send_email_lead_fail'];	
		
		if (isset($input['email_lead_fail']))
            $new_input['email_lead_fail'] = sanitize_text_field($input['email_lead_fail']);
		
		if (isset($input['ignore_customers_lead_fail']))
            $new_input['ignore_customers_lead_fail'] = sanitize_text_field($input['ignore_customers_lead_fail']);	

        if (isset($input['ignore_emails_lead_fail']))
            $new_input['ignore_emails_lead_fail'] = sanitize_text_field($input['ignore_emails_lead_fail']);	

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info() {
        print 'Enter your global default settings below, these can be overrode by the values in the shortcode:';
    }

    /**
     * Print the Section text
     */
    public function print_section_info_usage() {
        print 'Use shortcode to display ALC API Form, as below: <br/>
    	<pre>
    		[alcapi_contact_form]
    	</pre>
    	';
    }

    /**
     * Print the Section text
     */
    public function print_section_info_shortcode() {
        print 'The global default values above can be overrode by the shortcode usage, as below: <br/>
    	<pre>
    		[alcapi_contact_form geturl="http://applicationform.hatpacks.com.au/api/enquiryform"
    		posturl="http://applicationform.hatpacks.com.au/api/enquiryform/post"
    		cid="ALC"
    		css="https://www.australianlendingcentre.com.au/css/api-enquiry-form.css"]
    	</pre>
    	';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function geturl_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="geturl" name="alcapiform_option_name[geturl]" value="%s" />', isset($this->options['geturl']) ? esc_attr($this->options['geturl']) : 'http://applicationform.hatpacks.com.au/api/enquiryform'
        );
    }

    public function posturl_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="posturl" name="alcapiform_option_name[posturl]" value="%s" />', isset($this->options['posturl']) ? esc_attr($this->options['posturl']) : 'http://applicationform.hatpacks.com.au/api/enquiryform/post'
        );
    }

    public function cid_callback() {
        printf(
                '<input style="width: 80px;" type="text" id="cid" name="alcapiform_option_name[cid]" value="%s" />', isset($this->options['cid']) ? esc_attr($this->options['cid']) : 'ALC'
        );
    }

    public function svs_callback() {
        printf(
                '<input style="width: 80px;" type="text" id="svs" name="alcapiform_option_name[svs]" value="%s" />', isset($this->options['svs']) ? esc_attr($this->options['svs']) : '0'
        );
    }

    public function mode_callback() {
        printf(
                '<input style="width: 80px;" type="text" id="mode" name="alcapiform_option_name[mode]" value="%s" />', isset($this->options['mode']) ? esc_attr($this->options['mode']) : ''
        );
    }

    public function css_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[css]" value="%s" />', isset($this->options['css']) ? esc_attr($this->options['css']) : 'https://www.australianlendingcentre.com.au/css/api-enquiry-form.css'
        );
    }

    public function jquery_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[jquery]" value="%s" />', isset($this->options['jquery']) ? esc_attr($this->options['jquery']) : ''
        );
    }

    public function privacy_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[privacy]" value="%s" />', isset($this->options['privacy']) ? esc_attr($this->options['privacy']) : ''
        );
    }

    public function privacy_link_text_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[privacy_link_text]" value="%s" />', isset($this->options['privacy_link_text']) ? esc_attr($this->options['privacy_link_text']) : ''
        );
    }

    public function privacy_text_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[privacy_text]" value="%s" />', isset($this->options['privacy_text']) ? esc_attr($this->options['privacy_text']) : ''
        );
    }

    public function pssurl_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[pssurl]" value="%s" />', isset($this->options['pssurl']) ? esc_attr($this->options['pssurl']) : ''
        );
    }

    public function partner_email_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[partner_email]" value="%s" />', isset($this->options['partner_email']) ? esc_attr($this->options['partner_email']) : ''
        );
    }

    public function from_email_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[from_email]" value="%s" />', isset($this->options['from_email']) ? esc_attr($this->options['from_email']) : ''
        );
    }

    public function company_name_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[company_name]" value="%s" />', isset($this->options['company_name']) ? esc_attr($this->options['company_name']) : ''
        );
    }

    public function send_emails_callback() {
        printf(
                '<input type="checkbox" id="css" name="alcapiform_option_name[send_emails]" %s />', (isset($this->options["send_emails"]) && $this->options["send_emails"] == "on") ? "checked" : ""
        );
    }

    public function record_leads_callback() {
        printf(
                '<input type="checkbox" id="css" name="alcapiform_option_name[record_leads]" %s />', (isset($this->options["record_leads"]) && $this->options["record_leads"] == "on") ? "checked" : ""
        );
    }
	
	public function send_email_lead_fail_callback() {
        printf(
                '<input type="checkbox" id="css" name="alcapiform_option_name[send_email_lead_fail]" %s />', (isset($this->options["send_email_lead_fail"]) && $this->options["send_email_lead_fail"] == "on") ? "checked" : ""
        );
    }

	public function email_lead_fail_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[email_lead_fail]" value="%s" />', isset($this->options['email_lead_fail']) ? esc_attr($this->options['email_lead_fail']) : ''
        );
    }
	
	public function ignore_customers_lead_fail_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[ignore_customers_lead_fail]" value="%s" />', isset($this->options['ignore_customers_lead_fail']) ? esc_attr($this->options['ignore_customers_lead_fail']) : ''
        );
    }

    public function ignore_emails_lead_fail_callback() {
        printf(
                '<input style="width: 700px;" type="text" id="css" name="alcapiform_option_name[ignore_emails_lead_fail]" value="%s" />', isset($this->options['ignore_emails_lead_fail']) ? esc_attr($this->options['ignore_emails_lead_fail']) : ''
        );
    }

}
?>